# arrayrec

Arrayrec is a library to convert multidimensional array to array key value

## Installation

1- Clone the repository and instanciate the class.

2- Call the function "recursive_fiels" with an multidimensional array parameter

3- The function returns an array key <> value, where the keys are a breadcumb of multidimensional keys with # like glue


## Usage

```php
require_once('arrayrec.php');

$test=array( << VALUES >> );

$data = new arrayrec();
$results = $data->recursive_fiels($test);
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)